#include <QCoreApplication>
#include "Render3D/OpenGL/sfGlRenderer.hpp"
#include "PhysFsWrappers/Physfs4Cpp.hpp"
#include "Render3D/Vulkan/GlfwVulkan.hpp"
#include <fstream>

const std::string appname = "Test App";

int main(int argc, char* argv[]) {
	QCoreApplication a(argc, argv);
	PhysFs::FileHandle::Initialize(argv[0]);
	std::ifstream infile("mountlist.txt");
	std::string line;
	while (std::getline(infile, line))
	{
		PhysFs::FileHandle::Mount(line);
	}
	PhysFs::FileHandle::Mount(PhysFs::FileHandle::GetBaseDir() + PhysFs::FileHandle::GetDirSeparator() + "data" );
	std::string name = "Noname";
	/*GlfwVulkan vk(800,600,name);
	vk.Run();*/
	if( !InitVulkan() ) throw std::runtime_error("Unable to start Vulkan!");
	GlfwVulkan app(800,600,appname,VK_MAKE_VERSION(0,0,0));
	app.Run();
	/*sf::ContextSettings settings;
	settings.depthBits = 24;
	settings.stencilBits = 8;
	settings.antialiasingLevel = 8;
	settings.majorVersion = 3;
	settings.minorVersion = 3;
	settings.attributeFlags = settings.Core;
	sfGlRenderer render(sf::VideoMode(800, 600), "OpenGL",sf::Style::Default,settings);
	render.Run();*/
	PhysFs::FileHandle::Deinitialize();
	return 0;
	//return a.exec();
}
