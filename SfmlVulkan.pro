QT += core
QT -= gui

CONFIG += c++11

TARGET = SfmlVulkan
CONFIG += console
CONFIG -= app_bundle
LIBS += -L"/usr/lib/" -L"/usr/local/lib"
include(qmake_modules/findSFML.pro)
include(qmake_modules/findGLEW.pro)
include(qmake_modules/findGLFW.pro)
include(qmake_modules/findOpenGL.pro)
# include(qmake_modules/findVulkan.pro)
include(qmake_modules/findPhysFS.pro)
include(qmake_modules/findAssimp.pro)
include(qmake_modules/findLibraryLinker.pro)
INCLUDEPATH += $$PWD/external/glslang
DEPENDPATH += $$PWD/external/glslang
INCLUDEPATH += $$PWD/external/glslang/glslang/Include
#else:unix: LIBS += -L$$PWD/external/glslang

TEMPLATE = app

SOURCES += main.cpp \
    Render3D/AbstractRenderingEngine.cpp \
    Render3D/OpenGL/sfGlRenderer.cpp \
    Render3D/OpenGL/GlShader.cpp \
    Render3D/OpenGL/GlProgram.cpp \
    Render3D/VertexArray.cpp \
    Render3D/UploadableGpuArray.cpp \
    Render3D/IndexArray.cpp \
    Render3D/OpenGL/GlIndexBuffer.cpp \
    Render3D/OpenGL/GlVertexBuffer.cpp \
    Render3D/OpenGL/GlArrayBuffer.cpp \
    Render3D/OpenGL/GlVertexArray.cpp \
    PhysFsWrappers/PhySFML.cpp \
    PhysFsWrappers/PhAssimp.cpp \
    PhysFsWrappers/SoundBufferPhysfs.cpp \
    PhysFsWrappers/MusicWrapper.cpp \
    PhysFsWrappers/ImageWrapper.cpp \
    PhysFsWrappers/Physfs4Cpp.cpp \
    Render3D/OpenGL/AbstractGlFramework.cpp \
    Render3D/Vulkan/GlfwVkContainer.cpp \
    Render3D/Vulkan/VulkanWrapper.cpp \
    Render3D/Vulkan/GlfwVulkan.cpp \
    Render3D/Vulkan/VulkanFramebuffer.cpp \
    Render3D/Vulkan/AbstractVulkanEngine.cpp \
    Render3D/Vulkan/VkCommandBufferer.cpp \
    Render3D/Vulkan/VulkanArrayBuffer.cpp \
    Render3D/Vulkan/VulkanBuffer.cpp \
    Render3D/Vulkan/Device/VulkanAppContainer.cpp \
    Render3D/Vulkan/Device/VulkanInstance.cpp \
    Render3D/Vulkan/Device/VulkanPhysicalDevice.cpp \
    Render3D/Vulkan/Device/VulkanLogicalDevice.cpp \
    Render3D/Vulkan/Memory/VulkanMemory.cpp \
    Render3D/Vulkan/Pipeline/VulkanSwapChain.cpp \
    Render3D/Vulkan/Pipeline/VulkanShaderModule.cpp \
    Render3D/Vulkan/Pipeline/VulkanShader.cpp \
    Render3D/Vulkan/Pipeline/VulkanRenderPass.cpp \
    Render3D/Vulkan/Pipeline/VulkanPipelineLayout.cpp \
    Render3D/Vulkan/Pipeline/VulkanGraphicsPipeline.cpp \
    Render3D/Vulkan/VulkanResource.cpp \
    Render3D/Vulkan/Memory/VulkanMemoryPartition.cpp

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += \
    Render3D/AbstractRenderingEngine.hpp \
    Render3D/OpenGL/sfGlRenderer.hpp \
    Render3D/OpenGL/GlHeaders.hpp \
    Render3D/OpenGL/GlShader.hpp \
    Render3D/OpenGL/GlProgram.hpp \
    Render3D/UploadableGpuArray.hpp \
    Render3D/VertexArray.hpp \
    Render3D/IndexArray.hpp \
    Render3D/OpenGL/GlIndexBuffer.hpp \
    Render3D/OpenGL/GlVertexBuffer.hpp \
    Render3D/OpenGL/GlVertexArray.hpp \
    Render3D/OpenGL/GlArrayBuffer.hpp \
    PhysFsWrappers/PhySFML.hpp \
    PhysFsWrappers/PhAssimp.hpp \
    PhysFsWrappers/SoundBufferPhysfs.hpp \
    PhysFsWrappers/MusicWrapper.hpp \
    PhysFsWrappers/ImageWrapper.hpp \
    PhysFsWrappers/Physfs4Cpp.hpp \
    Render3D/OpenGL/AbstractGlFramework.hpp \
    global/global.hpp \
    Render3D/Vulkan/GlfwVkContainer.hpp \
    Render3D/Vulkan/VulkanWrapper.hpp \
    Render3D/Vulkan/GlfwVulkan.hpp \
    Render3D/Vulkan/VulkanResource.hpp \
    Render3D/Vulkan/VulkanFramebuffer.hpp \
    Render3D/Vulkan/AbstractVulkanEngine.hpp \
    Render3D/Vulkan/VkCommandBufferer.hpp \
    Render3D/Vulkan/VulkanArrayBuffer.hpp \
    Render3D/Vulkan/VulkanBuffer.hpp \
    Render3D/Vulkan/Device/VulkanInstance.hpp \
    Render3D/Vulkan/Device/VulkanPhysicalDevice.hpp \
    Render3D/Vulkan/Device/VulkanLogicalDevice.hpp \
    Render3D/Vulkan/Device/VulkanAppContainer.hpp \
    Render3D/Vulkan/Memory/VulkanMemory.hpp \
    Render3D/Vulkan/Memory/VulkanMemoryAllocator.hpp \
    Render3D/Vulkan/Pipeline/VulkanShaderModule.hpp \
    Render3D/Vulkan/Pipeline/VulkanSwapChain.hpp \
    Render3D/Vulkan/Pipeline/VulkanShader.hpp \
    Render3D/Vulkan/Pipeline/VulkanRenderPass.hpp \
    Render3D/Vulkan/Pipeline/VulkanPipelineLayout.hpp \
    Render3D/Vulkan/Pipeline/VulkanGraphicsPipeline.hpp \
    Render3D/Vulkan/Memory/VulkanMemoryPartition.hpp \
    global/linkedlist.hpp

DISTFILES += \
    vert.glsl \
    frag.glsl
