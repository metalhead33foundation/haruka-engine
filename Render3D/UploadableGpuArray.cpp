#include "UploadableGpuArray.hpp"
#include <stdexcept>

UploadableGpuArray::UploadableGpuArray()
{
	bUploadLockdown = false;
}
bool UploadableGpuArray::GetLocdown() const
{
	return bUploadLockdown;
}
void UploadableGpuArray::Upload()
{
	if(bUploadLockdown) throw std::runtime_error("Cannot upload array to GPU - already uploaded!");
	else
	{
		CleanArray();
		bUploadLockdown = true;
	}
}
size_t UploadableGpuArray::GetFullSize() const
{
	return GetArraySize() * GetIndividualSize();
}
