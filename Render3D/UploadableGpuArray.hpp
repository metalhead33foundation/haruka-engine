#ifndef UPLOADABLEGPUARRAY_HPP
#define UPLOADABLEGPUARRAY_HPP
#include <cstddef>

class UploadableGpuArray
{
private:
	bool bUploadLockdown;
protected:
	virtual void CleanArray() = 0;
public:
	UploadableGpuArray();
	bool GetLocdown() const;
	void Upload();
	virtual size_t GetArraySize() const = 0;
	virtual size_t GetIndividualSize() const = 0;
	virtual void* GetInitial() = 0;
	size_t GetFullSize() const;
};

#endif // UPLOADABLEGPUARRAY_HPP
