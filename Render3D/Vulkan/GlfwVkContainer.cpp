#include "GlfwVkContainer.hpp"

void GlfwVkContainer::initializeWindow()
{
	if (glfwCreateWindowSurface(getInstance()->getInstance(), window, nullptr, getSurfaceAddr()) != VK_SUCCESS)
		throw std::runtime_error("Couldn't create the window surface!");
}

GlfwVkContainer::GlfwVkContainer(uint width, uint height, const std::string& engine_name, uint32_t engine_version,
								 const std::string& appname, uint32_t app_version,CStringVector& desiredExtensions)
{
	this->width = width;
	this->height = height;
	CStringVector extensionVector;
	uint32_t extension_count;
	const char** extensions = glfwGetRequiredInstanceExtensions(&extension_count);
	for(uint32_t i = 0;i < extension_count;++i)
	{
		extensionVector.push_back(extensions[i]);
	}
	for(CStringIterator it = desiredExtensions.begin();it != desiredExtensions.end();++it)
	{
		extensionVector.push_back(*it);
	}
	CStringVector layerVector;
	createInstance(NULL, appname.c_str(),app_version, engine_name.c_str() , engine_version,
					VK_API_VERSION_1_0, NULL, 0,validationLayers,extensionVector,NULL);
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
	window = glfwCreateWindow(width, height, appname.c_str(), nullptr, nullptr);
	if(!window) throw std::runtime_error("Failed to initialize window!");
	initializeWindow();
}

sGlfwVkContainer GlfwVkContainer::create(uint width, uint height, const std::string& engine_name, uint32_t engine_version,
										 const std::string& appname,uint32_t app_version, CStringVector& desiredExtensions)
{
	return sGlfwVkContainer(new GlfwVkContainer(width, height, engine_name, engine_version,appname,app_version,
												 desiredExtensions));
}

GlfwVkContainer::~GlfwVkContainer()
{
	glfwDestroyWindow(window);
	//glfwTerminate();
}

Vk::pAppContainer GlfwVkContainer::castBack()
{
	//return std::static_pointer_cast<Vk::AppContainer>( std::make_shared<this> );
	//return std::make_shared<Vk::AppContainer>( static_cast<Vk::AppContainer*>(this) );
	return static_cast<Vk::pAppContainer>(this);
}
Vk::sAppContainer GlfwVkContainer::createCast(uint width, uint height, const std::string& engine_name, uint32_t engine_version, const std::string& appname,
									uint32_t app_version, CStringVector& desiredExtensions)
{
	return std::dynamic_pointer_cast<Vk::AppContainer>(create(width, height, engine_name, engine_version,appname,app_version,
												 desiredExtensions));
}
