#ifndef VKRESOURCE_HPP
#define VKRESOURCE_HPP
#include "Device/VulkanLogicalDevice.hpp"
#include <list>

namespace Vk {

DEFINE_CLASS(Resource)
class Resource
{
private:
	static sLogicalDevice singleton;
public:
	typedef std::list<pResource> resourceList;
	typedef resourceList::iterator resourceIterator;
	typedef resourceList::reverse_iterator resourceReverseIterator;

	static sLogicalDevice getSingleton();
	static void setSingleton(sLogicalDevice new_singleton);
	static void destroySingleton();
	static VkDevice getLogicalDevice();
	static sPhysicalDevice getPhysicalDevice();

	static resourceList list;
	static resourceIterator findResource(pResource res);
	static resourceReverseIterator findResourceBackwards(pResource res);
	resourceIterator getListPos();
	resourceReverseIterator getListRpos();

	Resource();
	virtual ~Resource();
};

}
#endif // VKRESOURCE_HPP
