#include "VulkanResource.hpp"
namespace Vk {

sLogicalDevice Resource::singleton = 0;
Resource::resourceList Resource::list(0);

sLogicalDevice Resource::getSingleton()
{
	if(singleton )return singleton;
	else throw std::runtime_error("The Vulkan device has not been initialized yet!");
}
void Resource::setSingleton(sLogicalDevice new_singleton)
{
	singleton = new_singleton;
}
void Resource::destroySingleton()
{
	IF_DEBUG(
		std::cout << "Removing all elements from the list before destroying the device.\n";
	)
	for(resourceIterator it = list.begin();it != list.end();++it)
	{
		delete *it;
	}
	IF_DEBUG(
				std::cout << "Succesfully removed all elements from the list.\n";
	)
	singleton.reset();
}
VkDevice Resource::getLogicalDevice()
{
	return getSingleton()->getDevice();
}
sPhysicalDevice Resource::getPhysicalDevice()
{
	return getSingleton()->getPhysicalDevice();
}
Resource::resourceIterator Resource::findResource(pResource res)
{
	for(resourceIterator it = list.begin();it != list.end();++it)
	{
		if(*it == res) return it;
	}
	throw false;
}
Resource::resourceReverseIterator Resource::findResourceBackwards(pResource res)
{
	for(resourceReverseIterator it = list.rbegin();it != list.rend();++it)
	{
		if(*it == res) return it;
	}
	throw false;
}
Resource::resourceIterator Resource::getListPos()
{
	return findResource(this);
}
Resource::resourceReverseIterator Resource::getListRpos()
{
	return findResourceBackwards(this);
}

Resource::Resource()
{
	IF_DEBUG(
				std::cout << "Adding the element: [" << reinterpret_cast<uint64_t>(this) << "] to the list.\n";
	)
	list.push_back(this);
	IF_DEBUG(
				std::cout << "Added the element: [" << reinterpret_cast<uint64_t>(this) << "] to the list.\n";
	)
}
Resource::~Resource()
{
	IF_DEBUG(
				std::cout << "Removing the element: [" << reinterpret_cast<uint64_t>(this) << "] from the list.\n";
	)
	list.remove(this);
	IF_DEBUG(
				std::cout << "Removed the element: [" << reinterpret_cast<uint64_t>(this) << "] from the list.\n";
	)
}

}
