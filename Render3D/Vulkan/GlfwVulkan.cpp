#include "GlfwVulkan.hpp"

const std::string GlfwVulkan::ENGINE_NAME = "Haruka Engine";
const uint32_t GlfwVulkan::ENGINE_VERSION = VK_MAKE_VERSION(1,0,0);

GlfwVulkan::GlfwVulkan(uint x, uint y, const std::string& name, uint32_t version)
{
	width = x;
	height = y;
	appname = name;
	app_version = version;
}
bool GlfwVulkan::Initialize()
{
	glfwInit();
	CStringVector desired_extensions(0);
	InitializeVulkan( GlfwVkContainer::createCast(width,height,ENGINE_NAME,ENGINE_VERSION,appname,app_version,desired_extensions) );
	return true;
}
void GlfwVulkan::Render(sf::Time deltaTime)
{
	if( glfwWindowShouldClose( std::static_pointer_cast<GlfwVkContainer>(container)->getWindow() ) )
	{
		exit_signal = true;
	}
	else
	{
		glfwPollEvents();
		drawFrame();
	}
}
void GlfwVulkan::Deinitialize()
{
	DeinitializeVulkan();
	//container.reset();
	glfwTerminate();
}
