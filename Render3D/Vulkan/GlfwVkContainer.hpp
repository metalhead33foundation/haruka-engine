#ifndef GLFWVKCONTAINER_HPP
#define GLFWVKCONTAINER_HPP
#include "Device/VulkanAppContainer.hpp"
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

DEFINE_CLASS(GlfwVkContainer)
class GlfwVkContainer : public Vk::AppContainer
{
private:
	GLFWwindow* window;
	uint width;
	uint height;
protected:
	void initializeWindow();
public:
	GlfwVkContainer(uint width, uint height, const std::string& engine_name, uint32_t engine_version, const std::string& appname,
					uint32_t app_version, CStringVector& desiredExtensions);
	static sGlfwVkContainer create(uint width, uint height, const std::string& engine_name, uint32_t engine_version, const std::string& appname,
								   uint32_t app_version, CStringVector& desiredExtensions);
	Vk::pAppContainer castBack();
	static Vk::sAppContainer createCast(uint width, uint height, const std::string& engine_name, uint32_t engine_version, const std::string& appname,
										uint32_t app_version, CStringVector& desiredExtensions);
	~GlfwVkContainer();
	uint getWidth() const { return width; }
	uint getHeight() const { return height; }
	GLFWwindow* getWindow() const { return window; }

};

#endif // GLFWVKCONTAINER_HPP
