#include "VulkanAppContainer.hpp"

namespace Vk {

const std::vector<const char*> AppContainer::validationLayers = {
	"VK_LAYER_LUNARG_standard_validation"
};

AppContainer::AppContainer()
{
	;
}
void AppContainer::setInstance(sInstance n_instance)
{
	instance = n_instance;
}
void AppContainer::createInstance(const void* pNextApp, const char* pApplicationName,
			uint32_t applicationVersion, const char* pEngineName, uint32_t engineVersion,
			uint32_t apiVersion, const void* pNext, VkInstanceCreateFlags flags,
			uint32_t enabledLayerCount,
			const char* const* ppEnabledLayerNames, uint32_t enabledExtensionCount,
			const char* const* ppEnabledExtensionNames, const VkAllocationCallbacks* pAllocator)
{
	instance = Instance::create(pNextApp,pApplicationName,applicationVersion,pEngineName,engineVersion,
								apiVersion,pNext,flags,enabledLayerCount,ppEnabledLayerNames,enabledExtensionCount,
								ppEnabledExtensionNames,pAllocator);
}
void AppContainer::createInstance(const void* pNextApp, const char* pApplicationName,
			uint32_t applicationVersion, const char* pEngineName, uint32_t engineVersion,
			uint32_t apiVersion, const void* pNext, VkInstanceCreateFlags flags,
			const std::vector<const char*>& ppEnabledLayerNames,
			const std::vector<const char*>& ppEnabledExtensionNames, const VkAllocationCallbacks* pAllocator)
{
	instance = Instance::create(pNextApp,pApplicationName,applicationVersion,pEngineName,engineVersion,
								apiVersion,pNext,flags,ppEnabledLayerNames,ppEnabledExtensionNames,pAllocator);
}
AppContainer::AppContainer::~AppContainer()
{
	vkDestroySurfaceKHR(instance->getInstance(),surface,0);
	instance.reset();
}

sInstance AppContainer::getInstance() const
{
	return instance;
}
const VkSurfaceKHR& AppContainer::getSurface() const
{
	return surface;
}
VkSurfaceKHR* AppContainer::getSurfaceAddr()
{
	return &surface;
}

}
