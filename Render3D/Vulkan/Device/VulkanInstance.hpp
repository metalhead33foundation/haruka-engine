#ifndef VULKANINSTANCE_HPP
#define VULKANINSTANCE_HPP
//#define GLFW_INCLUDE_VULKAN
//#include <GLFW/glfw3.h>
#include "../VulkanWrapper.hpp"
#include "../../global/global.hpp"

namespace Vk {

DEFINE_CLASS(Instance)
class Instance
{
private:
	VkInstance m_instance;
	const VkAllocationCallbacks* m_callback;
	Instance(const VkInstanceCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator=NULL);
public:
	~Instance();
	//VkInstance getRawInstance();
	const VkInstance& getInstance() const;
	VkInstance* getInstanceAddr();

	static sInstance create(const VkInstanceCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator=NULL);
	static sInstance create(const void* pNext, VkInstanceCreateFlags flags,
				const VkApplicationInfo* pApplicationInfo, uint32_t enabledLayerCount,
				const char* const* ppEnabledLayerNames, uint32_t enabledExtensionCount,
				const char* const* ppEnabledExtensionNames, const VkAllocationCallbacks* pAllocator=NULL);
	static sInstance create(const void* pNext, VkInstanceCreateFlags flags,
				const VkApplicationInfo* pApplicationInfo, const CStringVector& ppEnabledLayerNames,
				const CStringVector& ppEnabledExtensionNames, const VkAllocationCallbacks* pAllocator=NULL);
	static sInstance create(const void* pNextApp, const char* pApplicationName,
				uint32_t applicationVersion, const char* pEngineName, uint32_t engineVersion,
				uint32_t apiVersion, const void* pNext, VkInstanceCreateFlags flags,
				uint32_t enabledLayerCount,
				const char* const* ppEnabledLayerNames, uint32_t enabledExtensionCount,
				const char* const* ppEnabledExtensionNames, const VkAllocationCallbacks* pAllocator=NULL);
	static sInstance create(const void* pNextApp, const char* pApplicationName,
				uint32_t applicationVersion, const char* pEngineName, uint32_t engineVersion,
				uint32_t apiVersion, const void* pNext, VkInstanceCreateFlags flags,
				const CStringVector& ppEnabledLayerNames,
				const CStringVector& ppEnabledExtensionNames, const VkAllocationCallbacks* pAllocator=NULL);
};

}

#endif // VULKANINSTANCE_HPP
