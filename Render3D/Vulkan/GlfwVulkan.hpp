#ifndef GLFWVULKAN_HPP
#define GLFWVULKAN_HPP
#include "GlfwVkContainer.hpp"
#include "../AbstractRenderingEngine.hpp"
#include "AbstractVulkanEngine.hpp"

class GlfwVulkan : public AbstractRenderingEngine, public Vk::AbstractEngine
{
private:
	uint width;
	uint height;
	std::string appname;
	uint32_t app_version;
public:
	GlfwVulkan(uint x, uint y, const std::string& name, uint32_t version);
	virtual bool Initialize();
	virtual void Render(sf::Time deltaTime);
	virtual void Deinitialize();
	static const std::string ENGINE_NAME;
	static const uint32_t ENGINE_VERSION;
};

#endif // GLFWVULKAN_HPP
