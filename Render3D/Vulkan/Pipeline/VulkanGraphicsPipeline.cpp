#include "VulkanGraphicsPipeline.hpp"
namespace Vk {
GraphicsPipeline::GraphicsPipeline(sSwapChain swapchain, sShader shader)
{
	IF_DEBUG(
		std::cout << "Vk::GraphicsPipeline[" << reinterpret_cast<uint64_t>(this) << "]: Creating pipeline." << std::endl;
	)
	renderPass = RenderPass::create(swapchain);
	layout = PipelineLayout::create();
	VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
	vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertexInputInfo.vertexBindingDescriptionCount = 0;
	vertexInputInfo.vertexAttributeDescriptionCount = 0;

	VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
	inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	inputAssembly.primitiveRestartEnable = VK_FALSE;

	VkViewport viewport = {};
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = (float)swapchain->getDetails().extent.width;
	viewport.height = (float)swapchain->getDetails().extent.height;
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	VkRect2D scissor = {};
	scissor.offset = {0, 0};
	scissor.extent = swapchain->getDetails().extent;

	VkPipelineViewportStateCreateInfo viewportState = {};
	viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportState.viewportCount = 1;
	viewportState.pViewports = &viewport;
	viewportState.scissorCount = 1;
	viewportState.pScissors = &scissor;

	VkPipelineRasterizationStateCreateInfo rasterizer = {};
	rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizer.depthClampEnable = VK_FALSE;
	rasterizer.rasterizerDiscardEnable = VK_FALSE;
	rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
	rasterizer.lineWidth = 1.0f;
	rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
	rasterizer.frontFace = VK_FRONT_FACE_CLOCKWISE;
	rasterizer.depthBiasEnable = VK_FALSE;

	VkPipelineMultisampleStateCreateInfo multisampling = {};
	multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisampling.sampleShadingEnable = VK_FALSE;
	multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

	VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
	colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	colorBlendAttachment.blendEnable = VK_FALSE;

	VkPipelineColorBlendStateCreateInfo colorBlending = {};
	colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlending.logicOpEnable = VK_FALSE;
	colorBlending.logicOp = VK_LOGIC_OP_COPY;
	colorBlending.attachmentCount = 1;
	colorBlending.pAttachments = &colorBlendAttachment;
	colorBlending.blendConstants[0] = 0.0f;
	colorBlending.blendConstants[1] = 0.0f;
	colorBlending.blendConstants[2] = 0.0f;
	colorBlending.blendConstants[3] = 0.0f;

	VkGraphicsPipelineCreateInfo pipelineInfo = {};
	pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;

	if(!shader->isStaged()) shader->stage("main");

	pipelineInfo.stageCount = shader->getStageCount();
	pipelineInfo.pStages = shader->getStages();
	pipelineInfo.pVertexInputState = &vertexInputInfo;
	pipelineInfo.pInputAssemblyState = &inputAssembly;
	pipelineInfo.pViewportState = &viewportState;
	pipelineInfo.pRasterizationState = &rasterizer;
	pipelineInfo.pMultisampleState = &multisampling;
	pipelineInfo.pColorBlendState = &colorBlending;
	pipelineInfo.layout = layout->getPipelineLayout();
	pipelineInfo.renderPass = renderPass->getRenderPass();
	pipelineInfo.subpass = 0;
	pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;

	if (vkCreateGraphicsPipelines(getLogicalDevice(), VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &graphicsPipeline) != VK_SUCCESS) {
				throw std::runtime_error("failed to create graphics pipeline!");
	}
	shader->clear();
	IF_DEBUG(
		std::cout << "Vk::GraphicsPipeline[" << reinterpret_cast<uint64_t>(this) << "]: Successfully created pipeline." << std::endl;
	)
}
GraphicsPipeline::~GraphicsPipeline()
{
	IF_DEBUG(
		std::cout << "Vk::GraphicsPipeline[" << reinterpret_cast<uint64_t>(this) << "]: Destroying pipeline." << std::endl;
	)
	vkDestroyPipeline(getLogicalDevice(), graphicsPipeline, nullptr);
	layout.reset();
	renderPass.reset();
	IF_DEBUG(
		std::cout << "Vk::GraphicsPipeline[" << reinterpret_cast<uint64_t>(this) << "]: Destroyed pipeline." << std::endl;
	)
}
const VkPipeline& GraphicsPipeline::getPipeline() const
{
	return graphicsPipeline;
}
VkPipeline* GraphicsPipeline::getPipelineAddr()
{
	return &graphicsPipeline;
}
sPipelineLayout GraphicsPipeline::getLayout() const
{
	return layout;
}
sRenderPass GraphicsPipeline::getRenderPass() const
{
	return renderPass;
}
sGraphicsPipeline GraphicsPipeline::create(sSwapChain swapchain, sShader shader)
{
	return sGraphicsPipeline(new GraphicsPipeline(swapchain,shader));
}

}
