#ifndef VULKANSWAPCHAIN_HPP
#define VULKANSWAPCHAIN_HPP
#include "../VulkanResource.hpp"
namespace Vk {

DEFINE_CLASS(SwapChain)
class SwapChain : public Resource
{
private:
	VkSwapchainKHR swapChain;
	PhysicalDevice::SwapChainDetails details;
	std::vector<VkImage> swapChainImages;
	std::vector<VkImageView> swapChainImageViews;
	//std::vector<VkFramebuffer> swapChainFramebuffers;
public:
	void createFramebuffers();
	SwapChain(AppContainer* container);
	~SwapChain();
	static sSwapChain create(AppContainer* container);
	const VkSwapchainKHR& getSwapChain() const;
	VkSwapchainKHR* getSwapChainAddr();
	const PhysicalDevice::SwapChainDetails& getDetails() const;
	PhysicalDevice::SwapChainDetails* getDetailsAddr();
	const std::vector<VkImage>& getSwapChainImages() const;
	const std::vector<VkImageView>& getSwapChainImageViews() const;
	std::vector<VkImage>* getSwapChainImagesAddr();
	std::vector<VkImageView>* getSwapChainImageViewsAddr();
	//const std::vector<VkFramebuffer>& getSwapChainFramebuffers() const;
	//std::vector<VkFramebuffer>* getSwapChainFramebuffersAddr();

};

}
#endif // VULKANSWAPCHAIN_HPP
