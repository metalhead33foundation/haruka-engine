#ifndef VULKANMEMORY_HPP
#define VULKANMEMORY_HPP
#include "../VulkanResource.hpp"
#include "../../PhysFsWrappers/Physfs4Cpp.hpp"
namespace Vk {

DEFINE_CLASS(Memory)
class Memory : public Resource
{
private:
	VkDeviceMemory deviceMemory;
	volatile bool mapped;
public:
	const bool isMapped() const;
	void waitUntilFreed();

	Memory(const VkMemoryRequirements& memRequirements, VkMemoryPropertyFlags properties, const void* addr=0);
	//Memory(const VkMemoryRequirements& memRequirements,const void* addr,size_t size)
	~Memory();
	const VkDeviceMemory& getDeviceMemory() const;
	VkDeviceMemory* getDeviceMemoryAddr();
	void mapMemory(const void* addr,size_t size, size_t offset);
	void* startMapping(size_t size, size_t offset);
	void endMapping();
	void MapFile(PhysFs::FileHandle::SharedHandle handle, size_t offset, size_t maxsize=0);
	void MapFile(const char* filepath, size_t offset, size_t maxsize=0);
	void MapFile(const std::string& filepath, size_t offset, size_t maxsize=0);
	static sMemory create(const VkMemoryRequirements& memRequirements, VkMemoryPropertyFlags properties, const void* addr=0);
};

}
#endif // VULKANMEMORY_HPP
