#ifndef VULKANMEMORYPARTITION_HPP
#define VULKANMEMORYPARTITION_HPP
#include "VulkanMemory.hpp"
//#include <pair>
namespace Vk {

DEFINE_CLASS(MemoryPartition)
class MemoryPartition
{
public:
	typedef std::pair<bool,std::size_t> Chunk_T; // first = free or not | second = size
	typedef LinkedList<Chunk_T> Chunk;
	typedef Chunk* pChunk;
private:
	sMemory memspace;
	pChunk first_chunk;
	void refreshFirstChunk();
public:
	size_t measureOffsetUntil(pChunk chunkAddr);
	void* mapChunk(pChunk chunkAddr);
	void unmapChunk();
	pChunk claimMemory(size_t size);
	void mergeWithPrev(pChunk chunkAddr); // Use with caution
	void mergeWithNext(pChunk chunkAddr); // Use with caution
	void absorbFreed(pChunk chunkAddr);
};

}
#endif // VULKANMEMORYPARTITION_HPP
