#include "VulkanMemoryPartition.hpp"
namespace Vk {

void MemoryPartition::refreshFirstChunk()
{
	first_chunk = first_chunk->toTheStart();
}
size_t MemoryPartition::measureOffsetUntil(pChunk chunkAddr)
{
	size_t temp=0;
	pChunk head = first_chunk;
	// refreshFirstChunk();
	while(head && head != chunkAddr)
	{
		temp += head->element.second;
		head = head->next;
	}
	return temp;
}
void* MemoryPartition::mapChunk(pChunk chunkAddr)
{
	if(!chunkAddr) return 0;
	size_t offset = measureOffsetUntil(chunkAddr);
	memspace->waitUntilFreed();
	return memspace->startMapping(chunkAddr->element.second,offset);
}
void MemoryPartition::unmapChunk()
{
	memspace->endMapping();
}
MemoryPartition::pChunk MemoryPartition::claimMemory(size_t size)
{
	if(!first_chunk)
	{
		return new Chunk(Chunk_T(false,size));
	}
	pChunk head = first_chunk;
	while(head)
	{
		if(head->element.first && head->element.second >= size)
		{
			size_t remainder = head->element.second - size;
			if(!remainder)
			{
				head->element.first = false;
				return head;
			}
			else
			{
				pChunk temp = new Chunk(Chunk_T(false,size),head,true);
				head->element.second = remainder;
				if(head == first_chunk) first_chunk = temp;
				return temp;
			}
		}
		head = head->next;
	}
	head = first_chunk->toTheEnd();
	return new Chunk(Chunk_T(false,size),head,false);
}
void MemoryPartition::mergeWithPrev(pChunk chunkAddr)
{
	if(!chunkAddr) return;
	if(!chunkAddr->prev) return;
	if(chunkAddr->prev == first_chunk) first_chunk = chunkAddr;
	chunkAddr->element.second += chunkAddr->prev->element.second;
	delete chunkAddr->prev;
}
void MemoryPartition::mergeWithNext(pChunk chunkAddr)
{
	if(!chunkAddr) return;
	if(!chunkAddr->next) return;
	chunkAddr->element.second += chunkAddr->next->element.second;
	delete chunkAddr->next;
}
void MemoryPartition::absorbFreed(pChunk chunkAddr)
{
	if(!chunkAddr) return;
	if(!chunkAddr->element.first) return;
	while(chunkAddr->prev)
	{
		if(chunkAddr->prev->element.first) mergeWithPrev(chunkAddr);
		else break;
	}
	while(chunkAddr->next)
	{
		if(chunkAddr->next->element.first) mergeWithNext(chunkAddr);
		else break;
	}
	if(!chunkAddr->next) delete chunkAddr;
}

}
