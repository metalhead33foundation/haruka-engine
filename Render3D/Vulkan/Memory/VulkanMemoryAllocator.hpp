#ifndef VULKANMEMORYALLOCATOR_HPP
#define VULKANMEMORYALLOCATOR_HPP
#include "VulkanMemory.hpp"
namespace Vk {

template <class T>
struct MemoryAllocator
{
	typedef T value_type;
	T* allocate(std::size_t n);
	size_t offset;
	sMemory memspace;
	void deallocate(T* p, std::size_t);
	MemoryAllocator();
};
#define func_MemoryAllocator(ret,funcname) template <class T> ret MemoryAllocator<T>::funcname
func_MemoryAllocator(T*,allocate)(std::size_t n)
{
	memspace->waitUntilFreed();
	return static_cast<T*>(memspace->startMapping(n,offset));
}
func_MemoryAllocator(void,deallocate)(T* p, std::size_t)
{
	memspace->endMapping();
}
template <class T, class U>
bool operator==(const MemoryAllocator<T>&, const MemoryAllocator<U>&) { return true; }
template <class T, class U>
bool operator!=(const MemoryAllocator<T>&, const MemoryAllocator<U>&) { return false; }
#undef func_MemoryAllocator
}
#endif // VULKANMEMORYALLOCATOR_HPP
