#include "IndexArray.hpp"
#include <stdexcept>

IndexArray::IndexArray()
	: UploadableGpuArray()
{
	;
}
IndexArray::IndexArray(IndexArray* copier)
	: UploadableGpuArray()
{
	if(copier->GetLocdown()) throw std::runtime_error("You cannot copy from an already uploaded and locked down index array!");
	else
	{
		indices = copier->indices;
	}
}// Copy constructor
IndexArray::IndexArray(unsigned int* new_indices, size_t size)
	: UploadableGpuArray()
{
	UploadIndices(new_indices,size);
}
IndexArray::IndexArray(IndexVector& new_index)
	: UploadableGpuArray()
{
	UploadIndices(new_index);
}

void IndexArray::CleanArray()
{
	indices.clear();
}
size_t IndexArray::GetArraySize() const
{
	return indices.size();
}
size_t IndexArray::GetIndividualSize() const
{
	return sizeof(unsigned int);
}
void* IndexArray::GetInitial()
{
	return &indices[0];
}
void IndexArray::UploadIndices(unsigned int index)
{
	if(GetLocdown()) throw std::runtime_error("Cannot upload indices when you have already locked down the index array! (implying that you already uploaded the indices to the GPU and cleared out the array from the memory.)");
	else indices.push_back(index);
}
void IndexArray::UploadIndices(unsigned int* new_indices, size_t size)
{
	for(size_t i = 0;i < size;++i) UploadIndices(new_indices[i]);
}
void IndexArray::UploadIndices(IndexVector& new_index)
{
	for(IndexVector::iterator it = new_index.begin();it != new_index.end();++it)
		UploadIndices(*it);
}
