#ifndef ABSTRACTGLFRAMEWORK_HPP
#define ABSTRACTGLFRAMEWORK_HPP
#include "GlHeaders.hpp"
#include "GlShader.hpp"
#include "GlProgram.hpp"
#include "../../PhysFsWrappers/Physfs4Cpp.hpp"
#include <map>

class AbstractGlFramework
{
public:
	typedef std::map<std::string,GlShader::ShaderPointer> ShaderContainer;
	typedef std::map<std::string,GlProgram::ProgramPointer> ProgramContainer;
	//typedef GlShader::ShaderPointer (ShaderCreator)(std::string);
	typedef GlShader::ShaderPointer (*ShaderCreator)(std::string);
private:
	ShaderContainer m_shaders;
	ProgramContainer m_programs;
public:
	AbstractGlFramework();
	GlProgram::ProgramPointer getProgram(std::string name);
	GlShader::ShaderPointer getShader(std::string name);
	void createProgram(std::string name);
	void createShader(std::string name,std::string path,ShaderCreator func);
	void createComputeShader(std::string name,std::string path);
	void createVertexShader(std::string name,std::string path);
	void createFragmentShader(std::string name,std::string path);
	void createGeometryShader(std::string name,std::string path);
	void createTessalationControlShader(std::string name,std::string path);
	void createTessalationEvaluationShader(std::string name,std::string path);
	void attachShader(std::string program, std::string shader);
	void detachShader(std::string program, std::string shader);
	void linkProgram(std::string program);

	void deleteShader(std::string name);
	void deleteProgram(std::string name);
	void useProgram(std::string name);
};

#endif // ABSTRACTGLFRAMEWORK_HPP
