#ifndef SFGLRENDERER_HPP
#define SFGLRENDERER_HPP
#include "../AbstractRenderingEngine.hpp"
#include "GlHeaders.hpp"
#include "AbstractGlFramework.hpp"
#include <SFML/Graphics.hpp>
#include <map>

class sfGlRenderer : public AbstractRenderingEngine, public AbstractGlFramework
{
public:
	typedef std::map<std::string,GLuint> GlMap;
private:
	sf::RenderWindow m_window;
	// Wannabe-global variables
public:
	sfGlRenderer();
	bool Initialize();
	void Render(sf::Time deltaTime);
	void Deinitialize();
	sfGlRenderer(sf::VideoMode mode, const std::string &title, sf::Uint32 style=sf::Style::Default, const sf::ContextSettings &settings=sf::ContextSettings());
};

#endif // SFGLRENDERER_HPP
