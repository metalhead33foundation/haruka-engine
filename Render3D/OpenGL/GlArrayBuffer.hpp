#ifndef GLARRAYBUFFER_HPP
#define GLARRAYBUFFER_HPP
#include "GlHeaders.hpp"
#include "../UploadableGpuArray.hpp"

class GlArrayBuffer
{
private:
	GLenum eTarget; // Target
	UploadableGpuArray* m_buff;
protected:
	void GenerateBuffer();
	GLuint iBufferObject; // Buffer object
	size_t size;
	size_t elems;
public:
	GlArrayBuffer(GLenum nTarget, UploadableGpuArray* n_buff);
	~GlArrayBuffer();
	GLuint GetBufferObject();
	GLenum GetTarget();

	void Bind();
	virtual void OffloadToBuffer();
	void GenerateAllInOne();

	virtual void Draw() = 0;
	size_t GetElemnum() const;
};

#endif // GLARRAYBUFFER_HPP
