#ifndef GLVERTEXARRAY_HPP
#define GLVERTEXARRAY_HPP
#include "GlVertexBuffer.hpp"
#include <list>

class GlVertexArray
{
public:
	typedef std::list<GlVertexBuffer*> BufferList;
	GlVertexArray();
	~GlVertexArray();
	void AddBuffer(GlVertexBuffer* elem);
	void Initialize();
	void Bind();
	void Draw();
private:
	GLuint VertexArrayObject;
	BufferList mBufferList;
	size_t elems;
};

#endif // GLVERTEXARRAY_HPP
