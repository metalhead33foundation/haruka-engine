#ifndef GLINDEXBUFFER_HPP
#define GLINDEXBUFFER_HPP
#include "GlArrayBuffer.hpp"
#include "../IndexArray.hpp"

class GlIndexBuffer : public GlArrayBuffer
{
public:
	GlIndexBuffer(IndexArray* buf);
	void Draw();
};

#endif // GLINDEXBUFFER_HPP
