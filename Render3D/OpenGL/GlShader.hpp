#ifndef GLSHADER_HPP
#define GLSHADER_HPP
#include "GlHeaders.hpp"
#include <iostream>
#include <memory>
#include <vector>

class GlShader
{
private:
	GLuint m_shader;
protected:
	void compileShader();
public:
	typedef std::shared_ptr<GlShader> ShaderPointer;
	GlShader(GLenum shader_type, std::string source);
	GlShader(GLenum shader_type, const char* sauce, int len);
	GlShader(GLenum shader_type, std::vector<char>& sauce);
	~GlShader();
	GLuint getShader() const;
	// Static constructors - basic:
	static ShaderPointer createShader(GLenum shader_type, std::string source);
	static ShaderPointer createShader(GLenum shader_type, const char* sauce, int len);
	static ShaderPointer createShader(GLenum shader_type, std::vector<char>& sauce);
	// Static constructors - std::string
	static ShaderPointer createComputeShader(std::string sauce);
	static ShaderPointer createVertexShader(std::string sauce);
	static ShaderPointer createFragmentShader(std::string sauce);
	static ShaderPointer createGeometryShader(std::string sauce);
	static ShaderPointer createTessalationControlShader(std::string sauce);
	static ShaderPointer createTessalationEvaluationShader(std::string sauce);
	// Static constructors - const char
	static ShaderPointer createComputeShader(const char* sauce, int len);
	static ShaderPointer createVertexShader(const char* sauce, int len);
	static ShaderPointer createFragmentShader(const char* sauce, int len);
	static ShaderPointer createGeometryShader(const char* sauce, int len);
	static ShaderPointer createTessalationControlShader(const char* sauce, int len);
	static ShaderPointer createTessalationEvaluationShader(const char* sauce, int len);
	// Static constructors - std::vector<char>&
	static ShaderPointer createComputeShader(std::vector<char>& sauce);
	static ShaderPointer createVertexShader(std::vector<char>& sauce);
	static ShaderPointer createFragmentShader(std::vector<char>& sauce);
	static ShaderPointer createGeometryShader(std::vector<char>& sauce);
	static ShaderPointer createTessalationControlShader(std::vector<char>& sauce);
	static ShaderPointer createTessalationEvaluationShader(std::vector<char>& sauce);
};

#endif // GLSHADER_HPP
