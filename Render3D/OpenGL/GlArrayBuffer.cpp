#include "GlArrayBuffer.hpp"

GlArrayBuffer::GlArrayBuffer(GLenum nTarget, UploadableGpuArray* n_buff)
{
	iBufferObject = 0;
	eTarget = nTarget;
	m_buff = n_buff;
}
GLuint GlArrayBuffer::GetBufferObject()
{
	return iBufferObject;
}
void GlArrayBuffer::GenerateBuffer()
{
	glGenBuffers(1, &iBufferObject);
}
GLenum GlArrayBuffer::GetTarget()
{
	return eTarget;
}
void GlArrayBuffer::Bind()
{
	glBindBuffer(eTarget, iBufferObject);
}
void GlArrayBuffer::OffloadToBuffer()
{
	size = m_buff->GetFullSize();
	elems = m_buff->GetArraySize();
	glBufferData(eTarget,m_buff->GetFullSize(),m_buff->GetInitial(),GL_STATIC_DRAW);
}
void GlArrayBuffer::GenerateAllInOne()
{
	GenerateBuffer();
	Bind();
	OffloadToBuffer();
	m_buff->Upload();
}
GlArrayBuffer::~GlArrayBuffer()
{
	if(m_buff->GetLocdown()) glDeleteBuffers(1,&iBufferObject);
}
size_t GlArrayBuffer::GetElemnum() const
{
	return elems;
}
