#include "AbstractGlFramework.hpp"

AbstractGlFramework::AbstractGlFramework()
{

}
GlProgram::ProgramPointer AbstractGlFramework::getProgram(std::string name)
{
	return m_programs[name];
}
GlShader::ShaderPointer AbstractGlFramework::getShader(std::string name)
{
	return m_shaders[name];
}
void AbstractGlFramework::createShader(std::string name,std::string path,ShaderCreator func)
{
	m_shaders[name] = func(PhysFs::FileHandle::stringizeFile(path));
	//return m_shaders[name];
}
void AbstractGlFramework::createComputeShader(std::string name,std::string path)
{
	createShader(name,path,GlShader::createComputeShader);
}
void AbstractGlFramework::createVertexShader(std::string name,std::string path)
{
	createShader(name,path,GlShader::createVertexShader);
}
void AbstractGlFramework::createFragmentShader(std::string name,std::string path)
{
	createShader(name,path,GlShader::createFragmentShader);
}
void AbstractGlFramework::createGeometryShader(std::string name,std::string path)
{
	createShader(name,path,GlShader::createGeometryShader);
}
void AbstractGlFramework::createTessalationControlShader(std::string name,std::string path)
{
	createShader(name,path,GlShader::createTessalationControlShader);
}
void AbstractGlFramework::createTessalationEvaluationShader(std::string name,std::string path)
{
	createShader(name,path,GlShader::createTessalationEvaluationShader);
}
void AbstractGlFramework::attachShader(std::string program, std::string shader)
{
	m_programs[program]->attachShader(m_shaders[shader]);
}
void AbstractGlFramework::detachShader(std::string program, std::string shader)
{
	m_programs[program]->dettachShader(m_shaders[shader]);
}
void AbstractGlFramework::linkProgram(std::string program)
{
	m_programs[program]->link();
}
void AbstractGlFramework::createProgram(std::string name)
{
	m_programs[name] = GlProgram::createProgram();
}
void AbstractGlFramework::deleteShader(std::string name)
{
	m_shaders[name].reset();
}
void AbstractGlFramework::deleteProgram(std::string name)
{
	m_programs[name].reset();
}
void AbstractGlFramework::useProgram(std::string name)
{
	m_programs[name]->useProgram();
}
