#include "GlVertexBuffer.hpp"

GlVertexBuffer::GlVertexBuffer(VertexArray* vertices)
	: GlArrayBuffer(GL_ARRAY_BUFFER,vertices)
{
	;
}
void GlVertexBuffer::Draw()
{
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, elems/3);
}
