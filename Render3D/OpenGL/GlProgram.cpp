#include "GlProgram.hpp"
#include <vector>

GlProgram::GlProgram()
{
	m_program = glCreateProgram();
}
GlProgram::~GlProgram()
{
	glDeleteProgram(m_program);
}
GLuint GlProgram::getProgram() const
{
	return m_program;
}
void GlProgram::attachShader(GlShader* shader)
{
	glAttachShader(m_program,shader->getShader() );
}
void GlProgram::attachShader(GlShader::ShaderPointer shader)
{
	glAttachShader(m_program,shader->getShader() );
}
void GlProgram::dettachShader(GlShader* shader)
{
	glDetachShader(m_program,shader->getShader() );
}
void GlProgram::dettachShader(GlShader::ShaderPointer shader)
{
	glDetachShader(m_program,shader->getShader() );
}
void GlProgram::link()
{
	glLinkProgram(m_program);
	//Note the different functions here: glGetProgram* instead of glGetShader*.
	GLint isLinked = 0;
	glGetProgramiv(m_program, GL_LINK_STATUS, (int *)&isLinked);
	if(isLinked == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetProgramiv(m_program, GL_INFO_LOG_LENGTH, &maxLength);
		if(!maxLength) throw std::runtime_error("Something went wrong during the linking of the shader program and OpenGL is too much of a dickface to dedicate even one byte of information to tell us what.");

		//The maxLength includes the NULL character
		std::vector<GLchar> infoLog(maxLength);
		glGetProgramInfoLog(m_program, maxLength, &maxLength, &infoLog[0]);

		std::string error = std::string(&infoLog[0],infoLog.size() );
		infoLog.clear();
		throw std::runtime_error(error.c_str());
		//In this simple program, we'll just leave
	}
}
void GlProgram::useProgram()
{
	glUseProgram(m_program);
}
GlProgram::ProgramPointer GlProgram::createProgram()
{
	return ProgramPointer(new GlProgram());
}
