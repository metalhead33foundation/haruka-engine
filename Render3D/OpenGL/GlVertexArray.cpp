#include "GlVertexArray.hpp"

GlVertexArray::GlVertexArray()
{
	;
}
GlVertexArray::~GlVertexArray()
{
	for(BufferList::iterator it = mBufferList.begin();it != mBufferList.end();++it) delete *it;
	glDeleteVertexArrays(1,&VertexArrayObject);
}
void GlVertexArray::AddBuffer(GlVertexBuffer* elem)
{
	mBufferList.push_back(elem);
}
void GlVertexArray::Initialize()
{
	glGenVertexArrays(1,&VertexArrayObject);
	glBindVertexArray(VertexArrayObject);
	size_t i=0;
	for(BufferList::iterator it = mBufferList.begin();it != mBufferList.end();++it,++i)
	{
		(*it)->GenerateAllInOne();
		glEnableVertexAttribArray(i);
		glVertexAttribPointer(i, 3, GL_FLOAT, GL_FALSE, 0, 0);
	}
	elems = (*mBufferList.begin())->GetElemnum() / 3;
}
void GlVertexArray::Bind()
{
	glBindVertexArray(VertexArrayObject);
}
void GlVertexArray::Draw()
{
	glBindVertexArray(VertexArrayObject);
	glDrawArrays(GL_TRIANGLES,0,elems);
}
