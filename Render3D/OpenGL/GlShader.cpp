#include "GlShader.hpp"
#include <stdexcept>
#include <vector>

void GlShader::compileShader()
{
	glCompileShader(m_shader);
	GLint success = 0;
	glGetShaderiv(m_shader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		GLint maxLength = 0;
		glGetShaderiv(m_shader, GL_INFO_LOG_LENGTH, &maxLength);
		if(!maxLength) throw std::runtime_error("Something went wrong during the compilation of a shader and OpenGL is too much of a dickhead to dedicate even one byte of information to tell us what.");
		std::vector<GLchar> errorLog(maxLength);
		glGetShaderInfoLog(m_shader, maxLength, NULL, &errorLog[0]);
		std::string error = std::string(&errorLog[0], errorLog.size());
		glDeleteShader(m_shader);
		errorLog.clear();
		throw std::runtime_error(error.c_str());
	}
}

GlShader::GlShader(GLenum shader_type, std::string source)
{
	m_shader = glCreateShader(shader_type);
	const char* chara = source.c_str();
	int len = source.length();
	glShaderSource(m_shader,1,&chara,&len);
	compileShader();
}
GlShader::GlShader(GLenum shader_type, const char* sauce, int len)
{
	m_shader = glCreateShader(shader_type);
	glShaderSource(m_shader,1,&sauce,&len);
	compileShader();
}
GlShader::GlShader(GLenum shader_type, std::vector<char>& sauce)
{
	m_shader = glCreateShader(shader_type);
	char* chara = &sauce[0];
	int len = sauce.size();
	glShaderSource(m_shader,1,&chara,&len);
	compileShader();
}
GlShader::~GlShader()
{
	glDeleteShader(m_shader);
}
GLuint GlShader::getShader() const
{
	return m_shader;
}
GlShader::ShaderPointer GlShader::createShader(GLenum shader_type, std::string source)
{
	return ShaderPointer(new GlShader(shader_type,source));
}
GlShader::ShaderPointer GlShader::createShader(GLenum shader_type, const char* sauce, int len)
{
	return ShaderPointer(new GlShader(shader_type,sauce,len));
}
GlShader::ShaderPointer GlShader::createShader(GLenum shader_type, std::vector<char>& sauce)
{
	return ShaderPointer(new GlShader(shader_type,sauce));
}

GlShader::ShaderPointer GlShader::createComputeShader(std::string sauce)
{
	return createShader(GL_COMPUTE_SHADER,sauce);
}
GlShader::ShaderPointer GlShader::createVertexShader(std::string sauce)
{
	return createShader(GL_VERTEX_SHADER,sauce);
}
GlShader::ShaderPointer GlShader::createFragmentShader(std::string sauce)
{
	return createShader(GL_FRAGMENT_SHADER,sauce);
}
GlShader::ShaderPointer GlShader::createGeometryShader(std::string sauce)
{
	return createShader(GL_GEOMETRY_SHADER,sauce);
}
GlShader::ShaderPointer GlShader::createTessalationControlShader(std::string sauce)
{
	return createShader(GL_TESS_CONTROL_SHADER,sauce);
}
GlShader::ShaderPointer GlShader::createTessalationEvaluationShader(std::string sauce)
{
	return createShader(GL_TESS_EVALUATION_SHADER,sauce);
}
GlShader::ShaderPointer GlShader::createComputeShader(const char* sauce, int len)
{
	return createShader(GL_COMPUTE_SHADER,sauce,len);
}
GlShader::ShaderPointer GlShader::createVertexShader(const char* sauce, int len)
{
	return createShader(GL_VERTEX_SHADER,sauce,len);
}
GlShader::ShaderPointer GlShader::createFragmentShader(const char* sauce, int len)
{
	return createShader(GL_FRAGMENT_SHADER,sauce,len);
}
GlShader::ShaderPointer GlShader::createGeometryShader(const char* sauce, int len)
{
	return createShader(GL_GEOMETRY_SHADER,sauce,len);
}
GlShader::ShaderPointer GlShader::createTessalationControlShader(const char* sauce, int len)
{
	return createShader(GL_TESS_CONTROL_SHADER,sauce,len);
}
GlShader::ShaderPointer GlShader::createTessalationEvaluationShader(const char* sauce, int len)
{
	return createShader(GL_TESS_EVALUATION_SHADER,sauce,len);
}
GlShader::ShaderPointer GlShader::createComputeShader(std::vector<char>& sauce)
{
	return createShader(GL_COMPUTE_SHADER,sauce);
}
GlShader::ShaderPointer GlShader::createVertexShader(std::vector<char>& sauce)
{
	return createShader(GL_VERTEX_SHADER,sauce);
}
GlShader::ShaderPointer GlShader::createFragmentShader(std::vector<char>& sauce)
{
	return createShader(GL_FRAGMENT_SHADER,sauce);
}
GlShader::ShaderPointer GlShader::createGeometryShader(std::vector<char>& sauce)
{
	return createShader(GL_GEOMETRY_SHADER,sauce);
}
GlShader::ShaderPointer GlShader::createTessalationControlShader(std::vector<char>& sauce)
{
	return createShader(GL_TESS_CONTROL_SHADER,sauce);
}
GlShader::ShaderPointer GlShader::createTessalationEvaluationShader(std::vector<char>& sauce)
{
	return createShader(GL_TESS_EVALUATION_SHADER,sauce);
}
