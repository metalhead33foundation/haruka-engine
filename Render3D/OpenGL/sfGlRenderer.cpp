#include "sfGlRenderer.hpp"
#include <cstdio>
#include <exception>
#include <stdexcept>
#include <cstdlib>
#include <vector>
#include "GlVertexBuffer.hpp"
#include "GlVertexArray.hpp"

sfGlRenderer::sfGlRenderer(sf::VideoMode mode, const std::string &title, sf::Uint32 style, const sf::ContextSettings &settings)
	: m_window(mode,title,style,settings), AbstractRenderingEngine()
{
	printf("Initializing.\n");
	//m_mode = mode;
	//m_title = title;
	//m_style = style;
	//m_context = settings;
	//m_drawer = drawer;
}

float fTriangle[9];
float fTriangleColor[9];

GlVertexArray* arr;

bool sfGlRenderer::Initialize()
{
	GLenum err = glewInit();
	if(err != GLEW_OK)
	{
		printf("Unable to initialize GLEW.\n");
		return false;
	}
	try {
		// Setup triangle vertices
		fTriangle[0] = -0.4f; fTriangle[1] = 0.1f; fTriangle[2] = 0.0f;
		fTriangle[3] = 0.4f; fTriangle[4] = 0.1f; fTriangle[5] = 0.0f;
		fTriangle[6] = 0.0f; fTriangle[7] = 0.7f; fTriangle[8] = 0.0f;

		// Setup triangle color

		fTriangleColor[0] = 1.0f; fTriangleColor[1] = 0.0f; fTriangleColor[2] = 0.0f;
		fTriangleColor[3] = 0.0f; fTriangleColor[4] = 1.0f; fTriangleColor[5] = 0.0f;
		fTriangleColor[6] = 0.0f; fTriangleColor[7] = 0.0f; fTriangleColor[8] = 1.0f;

		VertexArray vertices(fTriangle,9);
		VertexArray vertex_colour(fTriangleColor,9);
		arr = new GlVertexArray();
		arr->AddBuffer(new GlVertexBuffer(&vertices));
		arr->AddBuffer(new GlVertexBuffer(&vertex_colour));
		arr->Initialize();
		createFragmentShader("frag","frag.glsl");
		createVertexShader("vert","vert.glsl");
		createProgram("prog");
		attachShader("prog","frag");
		attachShader("prog","vert");
		linkProgram("prog");
		useProgram("prog");
		return true;
	}
	catch(std::exception e)
	{
		std::cout << e.what() << std::endl;
		return false;
	}
}
void sfGlRenderer::Render(sf::Time deltaTime)
{
	sf::Event event;
	while (m_window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			// end the program
			exit_signal = true;
			//m_window.close();
		}
		else if (event.type == sf::Event::Resized)
		{
			// adjust the viewport when the window is resized
			glViewport(0, 0, event.size.width, event.size.height);
		}
	}
	/*if(!exit_signal)
	{*/
	m_window.setActive(true);

	arr->Draw();
	// end the current frame (internally swaps the front and back buffers)
	m_window.display();
	//}
}
void sfGlRenderer::Deinitialize()
{
	m_window.close();
	//delete m_window;
}
