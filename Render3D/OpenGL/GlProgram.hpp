#ifndef GLPROGRAM_HPP
#define GLPROGRAM_HPP
#include "GlShader.hpp"

class GlProgram
{
private:
	GLuint m_program;
public:
	typedef std::shared_ptr<GlProgram> ProgramPointer;
	static ProgramPointer createProgram();
	GlProgram();
	~GlProgram();
	GLuint getProgram() const;
	void attachShader(GlShader* shader);
	void attachShader(GlShader::ShaderPointer shader);
	void dettachShader(GlShader* shader);
	void dettachShader(GlShader::ShaderPointer shader);
	void link();
	void useProgram();
};

#endif // GLPROGRAM_HPP
