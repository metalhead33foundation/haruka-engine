#ifndef GLVERTEXBUFFER_HPP
#define GLVERTEXBUFFER_HPP
#include "GlArrayBuffer.hpp"
#include "../VertexArray.hpp"

class GlVertexBuffer : public GlArrayBuffer
{
public:
	GlVertexBuffer(VertexArray* vertices);
	void Draw();
};

#endif // GLVERTEXBUFFER_HPP
